FROM debian:10

ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF

LABEL maintainer="Davide 'argaar' Foschi" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$BUILD_VERSION \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.schema-version="1.0" \
      org.label-schema.vcs-url="https://gitlab.com/argaar/docker-debian-cordova.git" \
      org.label-schema.name="argaar/docker-debian-cordova" \
      org.label-schema.vendor="Davide Foschi (argaar)" \
      org.label-schema.description="Debian 10 with OpenJDK 8, NodeJS10, Android 29&30 and Cordova 10" \
      org.label-schema.url="https://gitlab.com/argaar/" \
      org.label-schema.license="MIT" \
      org.opencontainers.image.title="argaar/docker-debian-cordova" \
      org.opencontainers.image.description="Debian 10 with OpenJDK 8, NodeJS10, Android 29&30 and Cordova 10" \
      org.opencontainers.image.licenses="MIT" \
      org.opencontainers.image.authors="Davide Foschi (argaar)" \
      org.opencontainers.image.vendor="Davide Foschi (argaar)" \
      org.opencontainers.image.url="https://gitlab.com/argaar/docker-debian-cordova.git" \
      org.opencontainers.image.documentation="https://gitlab.com/argaar/docker-debian-cordova.git/blob/master/README.md" \
      org.opencontainers.image.source="https://gitlab.com/argaar/docker-debian-cordova.git"

ARG OPENJDK_VERSION="8u272-b10-0+deb9u1" \
    ANDROID_BUILD_TOOLS_VERSION=30.0.3 \
    ANDROID_SDK_VERSION_PREVIOUS=29 \
    ANDROID_SDK_VERSION_LAST=30 \
    GRADLE_VERSION=6.7.1 \
    ANDROID_CMDTOOLS_VERSION=6858069 \
    WGET_VERSION=1.20.1-1.1 \
    UNZIP_VERSION="6.0-23+deb10u1" \
    NPM_VERSION=6.14.9 \
    NODEJS_VERSION="10.21.0~dfsg-1~deb10u1" \
    GIT_VERSION=1:2.20.1-2+deb10u3 \
    CORDOVA_VERSION=10.0.0

ENV LANG="C.UTF-8" \
    DEBIAN_FRONTEND="noninteractive" \
    JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-amd64" \
    ANDROID_SDK_ROOT="/opt/android-sdk" \
    ANDROID_HOME="/opt/android-sdk" \
    GRADLE_HOME="/opt/gradle"

ENV PATH=${PATH}:${JAVA_HOME}/bin:${GRADLE_HOME}/bin:${ANDROID_SDK_ROOT}/cmdline-tools/bin:${ANDROID_SDK_ROOT}/tools:${ANDROID_SDK_ROOT}/platforms:${ANDROID_SDK_ROOT}/platform-tools:${ANDROID_SDK_ROOT}/emulator:${ANDROID_SDK_ROOT}/build-tools/${ANDROID_BUILD_TOOLS_VERSION}

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# System block, install jdk and other tools we need later
RUN echo "deb http://httpredir.debian.org/debian-security stretch/updates main" >/etc/apt/sources.list.d/stretch-updates.list \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
      ca-certificates \
      openjdk-8-jdk=${OPENJDK_VERSION} \
      wget=${WGET_VERSION} \
      unzip=${UNZIP_VERSION} \
      nodejs=${NODEJS_VERSION} \
      git=${GIT_VERSION} \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean

# Android block, gradle, cmd tools and sdk
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-all.zip -P /tmp \
    && unzip -d /opt /tmp/gradle-*.zip \
    && ln -s /opt/gradle-${GRADLE_VERSION} ${GRADLE_HOME} \
    && wget https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_CMDTOOLS_VERSION}_latest.zip -P /tmp \
    && mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools \
    && unzip -q -d ${ANDROID_SDK_ROOT} /tmp/commandlinetools-linux-${ANDROID_CMDTOOLS_VERSION}_latest.zip \
    && rm /tmp/commandlinetools-linux-${ANDROID_CMDTOOLS_VERSION}_latest.zip \
    && echo 'y' | ${ANDROID_SDK_ROOT}/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_SDK_ROOT} --licenses \
    && echo 'y' | ${ANDROID_SDK_ROOT}/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_SDK_ROOT} --install \
      "platform-tools" \
      "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
      "platforms;android-${ANDROID_SDK_VERSION_PREVIOUS}" \
      "platforms;android-${ANDROID_SDK_VERSION_LAST}" \
       "extras;android;m2repository" \
       "extras;google;google_play_services" \
       "extras;google;m2repository" \
    && rm -rf ${ANDROID_SDK_ROOT}/platform-tools-2 \
    && chmod a+x -R ${ANDROID_SDK_ROOT} \
    && chown -R root:root ${ANDROID_SDK_ROOT} \
    && rm -rf ${ANDROID_SDK_ROOT}/.android

# NPM, install it in order to install cordova
RUN wget https://registry.npmjs.org/npm/-/npm-${NPM_VERSION}.tgz -O /tmp/npm-${NPM_VERSION}.tgz \
    && mkdir -p /tmp/npm \
    && tar -xzf /tmp/npm-${NPM_VERSION}.tgz -C /tmp/npm \
    && /tmp/npm/package/bin/npm-cli.js install -gf /tmp/npm-${NPM_VERSION}.tgz

# Last block, cordova could be installed as every other dependencies has been installed
RUN npm install -g cordova@~${CORDOVA_VERSION} \
    && cordova telemetry off \
    && rm -rf /tmp/* /var/tmp/*